<?php
include "css/navbar.css.php";
require_once 'functions/init.functions.php';
?>

<!DOCTYPE html>
<html lang="hu">
<head>
    <title>Regisztráció</title>
</head>
<body>
<?php
if ( isset($_POST['submit']) )
{
    if ($_POST['password'] == $_POST['password2'])
    {
        $name = $_POST['name'];
        $email = $_POST['email'];
        $password =  md5($_POST['password']); // md5 password encryption
        $mobileNumber = ($_POST['mobileNumber'] ? $_POST['mobileNumber'] : '');
        $address = ($_POST['address'] ? $_POST['address'] : '');
        $birthday = $_POST['birthday'];

        //echo "Valtozok ellenorzese: " .$nev. " , " .$email. " , " .$password. " ,  " .$mobileNumber. " , " .$lakcim.  "" .$birthdayYear. "" .$birthdayMonth. "" .$birthdayDay. "";
        $user = User::create();
        $user->addData($name, $email, $password, $mobileNumber, $address, $birthday);
        $user->registerUser();
        $user->__destruct();
    }
    else
        echo "A ket jelszo nem egyezik meg";
}
?>

<div class="container">
    <h1>Regisztráció</h1>
</div>
<div class="container">
    <form action="register.php" method="post">

        <div class="form-group row">
            <label for="name" class="col-sm-3 col-form-label">Név: *</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" name="name" required maxlength="60" placeholder="Név">
            </div>
        </div>
        <div class="form-group row">
            <label for="email" class="col-sm-3 col-form-label">Email cím: *</label>
            <div class="col-sm-3">
                <input type="email" class="form-control" name="email" required maxlength="60" placeholder="Email cím">
            </div>
        </div>
        <div class="form-group row">
            <label for="password" class="col-sm-3 col-form-label">Jelszó: *</label>
            <div class="col-sm-3">
                <input type="password" class="form-control" name="password" required maxlength="60" placeholder="Jelszó">
            </div>
        </div>
        <div class="form-group row">
            <label for="password2" class="col-sm-3 col-form-label">Jelszó még egyszer: *</label>
            <div class="col-sm-3">
                <input type="password" class="form-control" name="password2" required maxlength="60" placeholder="Jelszó még egyszer">
            </div>
        </div>
        <div class="form-group row">
            <label for="mobileNumber" class="col-sm-3 col-form-label">Mobiltelefonszám:</label>
            <div class="col-sm-3">
                <input type="tel" class="form-control" name="mobileNumber" maxlength="15" placeholder="Mobiltelefonszám">
            </div>
        </div>
        <div class="form-group row">
            <label for="address" class="col-sm-3 col-form-label">Lakcím:</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" name="address" maxlength="60" placeholder="Lakcím">
            </div>
        </div>
        <div class="form-group row">
            <label for="birthday" class="col-sm-3 col-form-label">Születési dátum:</label>
            <div class="col-sm-3">
                <input type="date" class="form-control" name="birthday" max="<?php echo date("Y-m-d");?>">
            </div>
        </div>
        <div class="form-group row">
            <label for="userAgreement" class="col-sm-3 col-form-label">Felhasználói feltételek elfogadása</label>
            <div class="col-sm-3">
                <input type="checkbox" name="userAgreement" required>
            </div>
        </div>

            <input type="submit" class="btn btn-primary" name="submit" value="Regisztráció">
    </form>

</div>

</body>
</html>
