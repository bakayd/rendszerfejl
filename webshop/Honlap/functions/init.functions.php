<?php

//autoloader
spl_autoload_register('autoLoader');

function autoLoader($className) {
    $path = "classes/";
    $extension = ".class.php";
    $fullPath = $path . $className . $extension;

    require_once $fullPath;
}
?>