<script>
    $(document).ready(function() {
        
        var currenttype = "HUF";

        $('#huf').click(function(event) {
            if(currenttype=="USD"){
                changePrice(320,true);
            }
            else if(currenttype == "EUR"){
                changePrice(350,true);
            }
            currenttype = "HUF";
            var x = document.getElementsByClassName("pricetype");
            var i;
            for (i = 0; i < x.length; i++) {
                x[i].innerHTML = "Ft";
                x[i].value = "Ft";
            }
        });
        $('#usd').click(function(event) {
            if(currenttype=="HUF"){
                changePrice(1/320,false);
            }
            else if(currenttype == "EUR"){
                changePrice(350/320,false);
            }
            currenttype = "USD";
            var x = document.getElementsByClassName("pricetype");
            var i;
            for (i = 0; i < x.length; i++) {
                x[i].innerHTML = "$";
                x[i].value = "$";
            }
        });

        $('#eur').click(function(event) {
            if(currenttype=="HUF"){
                changePrice(1/350,false);
            }
            else if(currenttype == "USD"){
                changePrice(320/350,false);
            }
            currenttype = "EUR";
            var x = document.getElementsByClassName("pricetype");
            var i;
            for (i = 0; i < x.length; i++) {
                x[i].innerHTML = "€";
                x[i].value = "€";
            }
        });

        function changePrice(multiplyby,rounding){
            var x = document.getElementsByClassName("price");
            var i;
            for (i = 0; i < x.length; i++) {
                if(rounding)
                    x[i].innerHTML= Math.round(x[i].innerHTML*multiplyby);
                else
                    x[i].innerHTML= (x[i].innerHTML*multiplyby).toFixed(6);
            }
        }
    });
</script>