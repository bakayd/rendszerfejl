<!DOCTYPE html>
<html lang="hu">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <meta charset="UTF-8">
    <style>
        .navbar-brand{
            font-size: 30px;
        }
		.navbar .navbar-nav {
			display: inline-block;
			float: none;
		}
		.navbar .navbar-collapse {
			text-align: center;
        }
        .glyphicon-header,.glyphicon-usd,.glyphicon-euro{
            margin-right: 5px;
		}
    </style>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navBar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">Watera</a>
        </div>
        <div class="collapse navbar-collapse justified" id="navBar">
            <ul class="nav navbar-nav">
                <li class="nav-item"><a href="index.php">Kezdőlap</a></li>
                <li class="nav-item"><a href="login.php" id='login'>Bejelentkezés</a></li>
                <li class="nav-item">
                    <form method="post" class="navbar-form navbar-left" action="search.php" id="searchForm">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Keresés..." name="product">
							<div class="input-group-btn">
								<button type="submit" class="btn btn-default" name="submit">
									<i class="glyphicon glyphicon-search" id="searchIcon"></i>
								</button>                           
							</div>
                        </div>
						<a href="#" id="huf" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-header"></span>HUF</a>
						<a href="#" id="usd" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-usd"></span>USD</a>
						<a href="#" id="eur" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-euro"></span>EUR</a>
                    </form>
                </li>
            </ul>
        </div>
    </div>
</nav>
</body>
</html>

<?php
if (session_status() == 2 && isset($_SESSION['logged_in']))
    echo "<script>document.getElementById('login').innerHTML = 'Profilom';</script>";
?>

