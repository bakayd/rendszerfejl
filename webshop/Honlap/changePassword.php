<?php
ob_start();
session_start();
require_once 'functions/init.functions.php';
include "css/navbar.css.php";
?>

<!DOCTYPE html>
<html lang="hu">

<head>
    <title>Jelszó megváltoztatása</title>
</head>
<body>

    <div class="container" style="margin-bottom: 10px">
        <h1>Jelszó megváltoztatása</h1>
    </div>
    <div class="container">
        <form action="changePassword.php" method="post">

            <div class="form-group row">
                <label for="oldPassword" class="col-sm-2 col-form-label">Régi jelszó:</label>
                <div class="col-sm-3">
                    <input type="password" class="form-control" name="oldPassword" placeholder="Régi jelszó">
                </div>
            </div>

            <div class="form-group row">
                <label for="password" class="col-sm-2 col-form-label">Új jelszó:</label>
                <div class="col-sm-3">
                    <input type="password" class="form-control" name="password" required maxlength="60" placeholder="Új jelszó">
                </div>

            </div>

            <div class="form-group row">
                <label for="password2" class="col-sm-2 col-form-label">Új jelszó még egyszer:</label>
                <div class="col-sm-3">
                    <input type="password" class="form-control" name="password2" required maxlength="60" placeholder="Új jelszó még egyszer">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-10">
                    <input type="submit" class="btn btn-primary" name="submit" value="Jelszó megváltoztatása">
                </div>
            </div>
        </form>
    </div>
    <?php
    if ( isset($_POST['submit']) )
    {
        $conn = SqlConfig::connectToDatabase();
        $currUserID = $_SESSION['userID'];
        $result= $conn->query("SELECT password FROM registeredusers WHERE userID = '$currUserID'" );
        $row = $result->fetch_assoc();
        $oldPassword = $row['password'];
        if(md5($_POST['oldPassword']) == $oldPassword){ //ellenőrizni a jó-e a régi jelszó
            if ($_POST['password'] == $_POST['password2'])
            {
                $newPassword = md5($_POST['password']);
                $conn->query("UPDATE registeredusers SET password = '$newPassword' WHERE userID = '$currUserID'");
                header("Location:includes/successPasswordChange.includes.php");
                ob_end_flush();
            }
            else
                echo "<div class='container'><p class='bg-danger' style='width: 30%;text-align: center;'>A két jelszó nem egyezik meg.</p></div>";
        }
        else
            echo "<div class='container'><p class='bg-danger' style='width: 30%;text-align: center;'>Rossz régi jelszót adott meg.</p></div>";

        //ha sikerült a jelszóváltás átküldeni a successPasswordChange.includes.php oldalra
    }
    ?>
</body>
</html>