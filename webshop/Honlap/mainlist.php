<?php
include "css/mainlist.css.php";
include "script/currency.js.php";
require_once 'functions/init.functions.php';
$conn = SqlConfig::connectToDatabase();
?>

<!DOCTYPE html>
<html lang="hu">
<body>
    <div class="container">
        <div class="well well-sm">
            <strong>Megjelenítési mód:</strong>
            <div class="btn-group">
                <a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list"></span>Lista</a>
                <a href="#" id="grid" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th"></span>Rács</a>
            </div>
            <strong>Kategóriák:</strong>
            <div class="btn-group">
                <a href="#" id="all" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-large"></span>Mind</a>
                <?php
                $result = $conn->query("SELECT * FROM category");
                $cattable = $result->fetch_all(MYSQLI_NUM);
                foreach($cattable as $row){
                    $catID = "cat" . $row[0];
                    $categoryName = $row[1];
                    echo "<a href='#' id='$catID' class='btn btn-default btn-sm'><span class='glyphicon glyphicon-th-large'></span>$categoryName</a>";
                }
                ?>
            </div>
        </div>
        <div id="products" class="row list-group">
            <?php
            $currentdate = date('Y-m-d H:i:s');
			if (isset($_SESSION['logged_in'])) {
				$userID = $_SESSION['userID'];
				$result = $conn->query("SELECT * FROM products where endDate>='$currentdate' and userID!=$userID order by productID desc");
			} 
			else{
				$result = $conn->query("SELECT * FROM products where endDate>='$currentdate' order by productID desc");
			}
                if ($result->num_rows) {
                    $table = $result->fetch_all(MYSQLI_NUM);
                    foreach ($table as $row) { ?>
                        <div class="item col-xs-4 col-lg-4 itemcat<?php echo $row[4] ?>">
                            <div class="thumbnail">
                                <img class="group list-group-image" src= <?php echo "images/".$row[7] ?> alt="" />
                                <div class="caption">
                                    <h4 class="group inner list-group-item-heading">
                                        <?php echo $row[1] ?></h4>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6" style="display:table;">
                                            <div class="lead price" style="display: table-cell"><?php echo $row[2] ?></div><div class="lead pricetype" style="display: table-cell">Ft</div>
                                        </div>
                                        <div class="col-xs-12 col-md-6">
                                            <?php
                                            if (isset($_SESSION['logged_in'])) {
												$product_id = $row[0];
                                                echo "<a class='btn btn-success' href='productinfo.php?id=$product_id'>Megtekintés</a>";
                                            }else{
                                                echo "<a class='btn btn-success' href='register.php'>Regisztráció</a>";
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
            <?php
                    }
                } else {
                    echo '<div class="container"><h2>Nincs megjeleníthető termék! :(</h2></div>';
                }
                echo '</table>';
                $result->free();
            
            $conn->close();
            ?>
        </div>
    </div>
</body>
</html>

<script>
    $(document).ready(function() {
        $('#list').click(function(event) {
            event.preventDefault();
            $('#products .item').addClass('list-group-item');
        });
        $('#grid').click(function(event) {
            event.preventDefault();
            $('#products .item').removeClass('list-group-item');
            $('#products .item').addClass('grid-group-item');
        });

        <?php
            foreach($cattable as $row){
                $itemcatID = "itemcat" . $row[0];
                $catID2 = "cat" . $row[0];
                echo "var $itemcatID = document.getElementsByClassName('$itemcatID');\n\t\t";
                echo "$('#$catID2').click(function(event) {\n\t\t";
                foreach($cattable as $row){
                    $itemcatID2 = "itemcat" . $row[0];
                    echo "\thider($itemcatID2,";
                    if(substr($catID2,-1) == substr($itemcatID2,-1)){
                        echo "false";
                    }else{
                        echo "true";
                    }
                    echo ");\n\t\t";
                }
                echo "});\n\t\t";
            }
        ?>
        
        $('#all').click(function(event) {
            <?php
            foreach ($cattable as $row) {
                $itemcatIDAll = "itemcat" . $row[0];
                echo "hider($itemcatIDAll, false);\n";
            }
            ?>
        });
        
        function hider(item, hide) {
            if (hide) {
                for (let i = 0; i < item.length; i++) {
                    item[i].style.display = 'none';
                }
            } else {
                for (let i = 0; i < item.length; i++) {
                    item[i].style.display = "block";
                }
            }
        }
    });
</script>