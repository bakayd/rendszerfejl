<?php
session_start();
include "css/navbar.css.php";
include "css/mainlist.css.php";
include "script/currency.js.php";
require_once 'functions/init.functions.php';
$conn = SqlConfig::connectToDatabase();
?>

<!DOCTYPE html>
<html lang="hu">
<head>
    <title>Profil</title>
    <style>
        .btn-group2{
            float: right;
        }
        .profiledata{
            min-height: 200px;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="profiledata">
        <?php
        if (isset($_SESSION['logged_in'])) {
            echo "<b style=\"font-size:30px\">Üdvözöljük " . $_SESSION['userName'] . "! </b>";
        }
        ?>
        <br>
        <div class="btn-group2">
        <form action="addProduct.php" method="post" name="addProductForm">
            <input type="submit" class="btn btn-primary" name="addProduct" value="Termék hozzáadása">
        </form>
        <br>
        <form action="changePassword.php" method="post" name="changePasswordForm">
            <input type="submit" class="btn btn-primary" name="changePassword" value="Jelszó megváltoztatása">
        </form>
        <br>
        <form action="includes/logout.includes.php" method="post" name="logoutForm">
            <input type="submit" class="btn btn-primary" name="logout" value="Kijelentkezés">
        </form>
        </div>
        <?php
        $product = Product::create();
        //$product->getProduct(14);
        $product->getWonBids($_SESSION['userID']);
            //echo '<img src="images/laptop.jpg" alt="kép a termekről" style="height: 50%; width: 50%;"/>';

        ?>
        </div>
        <h2>Saját termékeim</h2>

        <div class="well well-sm">

            <strong>Megjelenítési mód:</strong>
            <div class="btn-group">
                <a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list"></span>Lista</a>
                <a href="#" id="grid" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th"></span>Rács</a>
            </div>
            <strong>Kategóriák:</strong>
            <div class="btn-group">
                <a href="#" id="all" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-large"></span>Mind</a>
                <?php
                $result = $conn->query("SELECT * FROM category");
                $cattable = $result->fetch_all(MYSQLI_NUM);
                foreach($cattable as $row){
                    $catID = "cat" . $row[0];
                    $categoryName = $row[1];
                    echo "<a href='#' id='$catID' class='btn btn-default btn-sm'><span class='glyphicon glyphicon-th-large'></span>$categoryName</a>";
                }
                ?>
            </div>
        </div>
        <div id="products" class="row list-group">
            <?php
            $currentdate = date('Y-m-d H:i:s');
            if ($result = $conn->query("SELECT * FROM products
            where userID = " . (string) $_SESSION['userID'] . " " .
                "order by productID desc")) {
                if ($result->num_rows) {
                    $table = $result->fetch_all(MYSQLI_NUM);
                    foreach ($table as $row) { ?>
                        <div class="item col-xs-4 col-lg-4 itemcat<?php echo $row[4] ?>">
                            <div class="thumbnail">
                                <img class="group list-group-image" src=<?php echo "images/".$row[7] ?> alt="" />
                                <div class="caption">
                                    <h4 class="group inner list-group-item-heading">
                                        <?php echo $row[1] ?></h4>
                                    <div class="row">
                                            <div class="col-xs-12 col-md-6" style="display:table;">
                                                <div class="lead price" style="display: table-cell"><?php echo $row[2] ?></div><div class="lead pricetype" style="display: table-cell">Ft</div>
                                            </div>
                                            <div class="col-md-4">
                                            <?php
                                                $product_id = $row[0];
                                                echo "<a class='btn btn-success' href='productinfo.php?id=$product_id'>Megtekintés</a>";
                                            ?>
                                            </div>
                                            <div class="col-md-6">
                                                <?php
                                                    if ($row[6] <= $currentdate)
                                                    echo "<p class='bg-danger text-center'>Elkelt</p>";
                                                ?>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
            <?php
                    }
                } else {
                    echo '<div class="container"><h2>Nincs megjeleníthető termék! :(</h2></div>';
                }
                echo '</table>';
                $result->free();
            }
            $conn->close();
            ?>
        </div>
    </div>
</body>

</html>

<script>
    $(document).ready(function() {

        $('#list').click(function(event) {
            event.preventDefault();
            $('#products .item').addClass('list-group-item');
        });
        $('#grid').click(function(event) {
            event.preventDefault();
            $('#products .item').removeClass('list-group-item');
            $('#products .item').addClass('grid-group-item');
        });
        
        <?php
            foreach($cattable as $row){
                $itemcatID = "itemcat" . $row[0];
                $catID2 = "cat" . $row[0];
                echo "var $itemcatID = document.getElementsByClassName('$itemcatID');\n\t\t";
                echo "$('#$catID2').click(function(event) {\n\t\t";
                foreach($cattable as $row){
                    $itemcatID2 = "itemcat" . $row[0];
                    echo "\thider($itemcatID2,";
                    if(substr($catID2,-1) == substr($itemcatID2,-1)){
                        echo "false";
                    }else{
                        echo "true";
                    }
                    echo ");\n\t\t";
                }
                echo "});\n\t\t";
            }
        ?>
        
        $('#all').click(function(event) {
            <?php
            foreach ($cattable as $row) {
                $itemcatIDAll = "itemcat" . $row[0];
                echo "hider($itemcatIDAll, false);\n";
            }
            ?>
        });

        function hider(item, hide) {
            if (hide) {
                for (let i = 0; i < item.length; i++) {
                    item[i].style.display = 'none';
                }
            } else {
                for (let i = 0; i < item.length; i++) {
                    item[i].style.display = "block";
                }
            }
        }
    });
</script>