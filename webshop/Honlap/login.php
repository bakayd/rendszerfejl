<?php
session_start();
include "css/navbar.css.php";
require_once 'functions/init.functions.php';
?>

<!DOCTYPE html>
<html lang="hu">
<head>
    <title>Login</title>
</head>
<body>
<div class="container" style="margin-bottom: 10px">
    <h1>Bejelentkezés</h1>
</div>
<div class="container">
    <form action="login.php" method="post" name="form" >

        <div class="form-group row">
            <label for="email" class="col-sm-1 col-form-label">Email:</label>
            <div class="col-sm-3">
                <input type="email" class="form-control" name="email" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="password" class="col-sm-1 col-form-label">Jelszó:</label>
            <div class="col-sm-3">
                <input type="password" class="form-control" name="password" required>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-10">
                <input type="submit" class="btn btn-primary" name="submit" value="Bejelentkezés">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <label for="ujFiok">Nincs fiókod? Hozz létre egyet:</label>
                <a href="register.php">Fiók létrehozása</a>
            </div>

        </div>
    </form>
</div>
<?php

if ( isset($_POST['submit']) )
{
    $email = $_POST['email'];
    $password = $_POST['password'];

    $user = User::create();
    $user->login($email, $password);
    $user->__destruct();
}

if (session_status() == 2 && isset($_SESSION['logged_in'])){
    header("Location: profile.php");
}

?>

</body>
</html>