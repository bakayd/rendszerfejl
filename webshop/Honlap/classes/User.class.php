<?php

require_once "SqlConfig.class.php";

class User{

    private $userId;
    private $name;
    private $email;
    private $password;
    private $mobileNumber;
    private $address;
    private $birthday;

    // __construct helyett fillData
    // es egyures construktor kell majd
    public function addData($name, $email, $password, $mobileNumber, $address, $birthday)
    {
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
        $this->mobileNumber = $mobileNumber;
        $this->address = $address;
        $this->birthday = $birthday;
    }

    public static function create() // empty consttruct
    {
        $instance = new self();
        return $instance;
    }

    public function __destruct() {}

    public function registerUser()
    {
        $conn = SqlConfig::connectToDatabase();
        $matchMail = $conn->query("SELECT * FROM registeredusers WHERE email='$this->email'");

        if ( $matchMail->num_rows > 0 )
            echo "Már van regisztrálva felhasználó az alábbi email címmel: " .$this->email. "";
        else
        {
            $this->name = $conn->real_escape_string($this->name);
            $this->email = $conn->real_escape_string($this->email);
            $this->address = $conn->real_escape_string($this->address);
            
            $sql = "INSERT INTO registeredusers (UserName, email, password, mobileNumber, address, birthday) 
                    VALUES ('$this->name', '$this->email', '$this->password', '$this->mobileNumber', '$this->address', '$this->birthday')";

            if(mysqli_query($conn, $sql)){
                header("Location: includes/successRegistration.includes.php");

            } else
                echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
        }
        $conn->close();
    }

    public function login($email, $password)
    {
        $conn = SqlConfig::connectToDatabase();
        $sqlResult = $conn->query("SELECT * FROM registeredusers WHERE email='$email'");

        if ($sqlResult->num_rows == 0) {
            echo "<div class='container'><p class='bg-danger' style='width: 30%;text-align: center'>Nincs ilyen felhasználó.</p></div>";
        }
        else
        {
            $userData = $sqlResult->fetch_assoc();

            if (md5($password) == $userData['password'])
            {
                $_SESSION['userID'] = $userData['userID'];
                $_SESSION['userName'] = $userData['userName'];
                $_SESSION['email'] = $userData['email'];
                $_SESSION['password'] = $userData['password'];
                $_SESSION['mobileNumber'] = $userData['mobileNumber'];
                $_SESSION['address'] = $userData['address'];
                $_SESSION['birthday'] = $userData['birthday'];
                $_SESSION['logged_in'] = true;
                header("Location: profile.php");
            }
            else
            {
                echo "<div class='container'><p class='bg-danger' style='width: 30%;text-align: center'>Rossz jelszót adott meg.</p></div>";
            }

        }
    }

    public function setUserId($userId) {
        $this->userId = $userId;
    }

    public function getUserID()
    {
        return $this->userId;
    }

} // class END
?>