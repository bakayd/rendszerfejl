<?php

require_once "SqlConfig.class.php";
date_default_timezone_set('Europe/Budapest');

//INSERT INTO `abc`
//(`img`)
//VALUES
//(LOAD_FILE('C:/Users/adity/Desktop/New folder/a.png'))


    class Product {

    private $productID;
    private $productName;
    private $price;
    private $description;
    private $category;
    private $endDate;
    private $image;
    private $userID;
    private $isActive;
    private $highestBid;
    private $highestBidUserID;

    public function addData($productName, $price, $description, $category, $endDate, $image, $userID)
    {
        $this->productName = $productName;
        $this->price = $price;
        $this->description = $description;
        $this->category = $category;
        $this->endDate = $endDate;
        $this->image = $image;
        $this->userID = $userID;
    }

    public static function create() // empty consttruct
    {
        $instance = new self();
        return $instance;
    }

    public function getProductFromDB() //todo ide az sql statment
    {
        $conn = SqlConfig::connectToDatabase();
        $sqlResult = $conn->query("SELECT * FROM products WHERE");
    }

    public function addProduct()
    {
        $conn = SqlConfig::connectToDatabase();

        //karakter escape
        $this->productName = $conn->real_escape_string($this->productName);
        $this->description = $conn->real_escape_string($this->description);

        $sql = "INSERT INTO products (productName, price, description, categoryID, userID, endDate, image)
                VALUES ('$this->productName', '$this->price', '$this->description', '$this->category', '$this->userID' , '$this->endDate','$this->image')";
        //$sql = "INSERT INTO products (productName, price, description, categoryID, userID, endDate, image)
        //VALUES ('szoveg', '325324', 'leiras', '1', '1' , '2020-04-30 12:00:00', '$this->image')";

        if(mysqli_query($conn, $sql))
            header("Location: includes/successProductAdd.includes.php");

        else
            echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
    }

    public function getProduct($productID)
    {
        $conn = SqlConfig::connectToDatabase();
        $sqlResult = $conn->query("SELECT * FROM products WHERE productID='$productID'");

        if ( $sqlResult->num_rows == 0 )
            echo "Nincs ilyen termék";
        else
        {
            $productData = $sqlResult->fetch_assoc();

            $productID = $productData['productID'];
            $productName = $productData['productName'];
            $price = $productData['price'];
            $description = $productData['description'];
            $categoryID = $productData['categoryID'];
            $userID = $productData['userID'];

            $endDate = $productData['endDate'];
            $image = 'images/'.$productData['image'];
            $payed = $productData['payed'];

            print('
                <div class="container">
                    <div class="row">

                        <div class="col-md-5 well well-sm">
                            <img class="img-responsive " src=' . $image . ' alt="kép a termékről"/>
                        </div>

                        <div class="col-md-7">
                            <div class="col-md-10 well well-sm">
                
                                    <div class="row">
                                        <div class="col-md-12" style="margin-bottom: 10px">
                                            <div class="col-md-12">
                                                <h1>' . $productName . '</h1>
                                            </div>
                                            <hr style="width: 100%;
                                            color: black;
                                            height: 1px;
                                            background-color:black;">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-5">
                                                <p class="lead">Fizetendő ár:</p>
                                            </div>
                                            <div class="col-md-4">
                                                <p class="lead price">'.$price.'</p>
                                            </div>
                                            <div class="col-md-1">
                                                <p class="lead pricetype">Ft</p>
                                            </div>
                                            <hr style="width: 100%;
                                                color: black;
                                                height: 1px;
                                                background-color:black;">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-5">
                                                <p class="lead">Leírás:</p>
                                            </div>
                                            <div class="col-md-7" style="margin-bottom: 10px">
                                                '.$description.'
                                            </div>
                                            <hr style="width: 100%;
                                                color: black;
                                                height: 1px;
                                                background-color:black;">
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-5">
                                                <p class="lead">Licit vége:</p>
                                            </div>
                                            <div class="col-md-7">
                                                <p class="lead">'.$endDate.'</p>
                                                <p class="small text-center" id="bidtimer"></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            ');
        }
    }

    public function getWonBids($userID)
    {
        echo '<br>';
        echo '<b style="font-size:30px">Megnyert licit(ek): </b> ' . '<br>';
        //echo "Teszt user ID" . $userID . '<br>';

        // kivalasztjuk az osszes licitet, ahol a felhasználó vezet
        $conn = SqlConfig::connectToDatabase();
        $allBids = $conn->query("SELECT * FROM bid WHERE userID='$userID'");
        $table = $allBids->fetch_all(MYSQLI_NUM);

        foreach ($table as $row)
        {
            $wonProductID = $row[1];

            // és a termekek listabol csak ezek a termekek lesznek kiválasztva
            $sqlResult = $conn->query("SELECT * FROM products WHERE productID='$wonProductID'");
            $table2 = $sqlResult->fetch_all(MYSQLI_NUM);

            foreach ($table2 as $row)
            {
                $now = new DateTime("now");
                $dateDB = $row[6];
                $bidEndDate = new DateTime($dateDB);

                // ellenörizzük, hogy lejárt -e
                if ($now > $bidEndDate)
                {
                    $wonProductName = $row[1];

                    //echo 'Won products: ' . $wonProductID . '<br>';
                    //echo '<b>Termék kiir teszt. </b> ID: ' .$row[0] . ' productName: '. $row[1]. ' price: ' . $row[2] . ' description: ' . $row[3]
                    //    . 'categori id: '. $row[4] . " User ID: " . $row[5] . " DATUM: " . $row[6] . '<br>';

                    $sqlResult = $conn->query("SELECT * FROM bid WHERE productID = '$wonProductID'");

                   if ( $sqlResult->num_rows == 0 )
                        echo "Nincs ilyen termék";
                    else
                    {

                        $_SESSION['productID'] = $wonProductID;

                        echo '<b> Megnyert termék neve: </b>' . $wonProductName . ' ';
                        $getWonBidPriceSql = $conn->query("SELECT bidID,bid FROM bid WHERE productID='$wonProductID'");
                        $getWonBidPrice = mysqli_fetch_row($getWonBidPriceSql);
                        echo "<div style='display: table'><b>Termék ára:</b>";
                        echo "<div class='price' style='display: table-cell'>" . $getWonBidPrice[1] . "</div><div class='pricetype' style='display: table-cell'>Ft</div></div>";

                        $_SESSION['bidID'] = $getWonBidPrice[0];
                        $_SESSION['productID'] = $wonProductID;

                        echo '
                        <form action="paying.php" method="post" name="payForProduct">
                            <input type="submit" class="btn btn-primary" name="payForProduct" value="Termék kifizetése">
                            ';
                        echo '<br>';
                    }
                }
            }
        }
    } // function END

    public function payForProduct($bidID, $productID)
    {
        //$conn = SqlConfig::connectToDatabase();
        /*
        $sqlResult = $conn->query("SELECT * FROM products WHERE bidID='$bidID'");

        $productData = $sqlResult->fetch_assoc();
        $price = $productData['price'];

        echo "Kifizetve: " . $price. '<br>';
        */
        //$conn->query("DELETE FROM bid WHERE bidID='$bidID'");
        //$conn->query("DELETE FROM products WHERE productID='$productID'");
        $this->deleteBid($bidID);
        $this->deleteProduct($productID);
        header("Location: includes/successPaying.includes.php");
    }

    public function deleteBid($bidID)
    {
        $conn = SqlConfig::connectToDatabase();
        $conn->query("DELETE FROM bid WHERE bidID='$bidID'");
    }

    public function deleteProduct($productID)
    {
        $conn = SqlConfig::connectToDatabase();
        $conn->query("DELETE FROM products WHERE productID='$productID'");
    }


    } // class END
?>