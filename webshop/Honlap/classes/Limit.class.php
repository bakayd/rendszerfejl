<?php
require_once "SqlConfig.class.php";

require 'includes/PHPMailer.php';
require 'includes/Exception.php';
require 'includes/SMTP.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception ;

class Limit {

    private $limitID;
    private $userID;
    private $productID;
    private $bidID;
    private $value;

    public static function create()
    {
        $instance = new self();
        return $instance;
    }

    public function addData($limitID, $userID, $productID, $bidID, $value)
    {
        $this->limitID = $limitID;
        $this->userID = $userID;
        $this->productID = $productID;
        $this->bidID = $bidID;
        $this->value = $value;
    }

    public function addLimit($limitValue, $userID, $productID)
    {
        //echo "Add limit function!" . '<br>';
        if ($limitValue != "")
        {
            $conn = SqlConfig::connectToDatabase();
            $sqlResult = $conn->query("SELECT * FROM bid WHERE userID='$userID' and productID='$productID'");

            $table = $sqlResult->fetch_all(MYSQLI_NUM);

            foreach ($table as $row)
                $bidID = $row[0];

            $updateIfExists = $conn->query("SELECT * FROM limitTable 
                                                  WHERE userID='$userID' and productID='$productID'");

            if ($updateIfExists->num_rows > 0)
            {
                $sql = "UPDATE limitTable 
                        SET value = $limitValue 
                        WHERE userID='$userID' and productID='$productID'";
            }
            else
            {
                $sql = "INSERT INTO limitTable (userID, productID, bidID, value)
                        VALUES ('$userID', '$productID', $bidID, $limitValue)";
            }

            if(mysqli_query($conn, $sql))
            {
                //echo 'Kérem várjon...';
                //header("Location: includes/successProductAdd.includes.php");
            }
            else
                echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
        }
        else
        {
            $conn = SqlConfig::connectToDatabase();
/*            $sqlResult = $conn->query("SELECT * FROM bid WHERE userID='$userID' and productID='$productID'");

            $table = $sqlResult->fetch_all(MYSQLI_NUM);

            foreach ($table as $row)
                $bidID = $row[0];*/

            $updateIfExists = $conn->query("SELECT * FROM limitTable 
                                                  WHERE userID='$userID' and productID='$productID'");

            if ($updateIfExists->num_rows > 0)
            {
                $sql = "DELETE FROM limitTable
                        WHERE userID='$userID' and productID='$productID'";

                if(mysqli_query($conn, $sql))
                {
                    //echo 'Kérem várjon...';
                    //header("Location: includes/successProductAdd.includes.php");
                }
                else
                    echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
            }
        }
    }

    public function limitReachedNotify($price, $userID, $productID)
    {
        $conn = SqlConfig::connectToDatabase();
        $sqlResult = $conn->query("SELECT * FROM limitTable WHERE userID != '$userID' and productID = '$productID'");
        $table = $sqlResult->fetch_all(MYSQLI_NUM);

        //echo "Notifiy teszt";

        foreach ($table as $row)
        {
            $userID2 = $row[1];
            $productID2 = $row[2];
            $bidID2 = $row[3];
            $value2 = $row[4];

            $bidValueResult = $conn->query("SELECT bid FROM bid WHERE bidID='$bidID2'");
            $bidValue2 = mysqli_fetch_row($bidValueResult);

            $limitValueResult = $conn->query("SELECT value FROM limitTable WHERE bidID='$bidID2'");
            $limitValue2 = mysqli_fetch_row($limitValueResult);

            echo "Valtozok ellenorzese: User id:" . $userID2 . " product id" . $productID2 . " bid id" . $bidID2 . " hatérték" .$value2 . '<br>';
            echo "Teszt" . ' BID: ' . $bidValue2[0] .'<br>';
            echo "LIMIT: " . $limitValue2[0] . '<br>';
            
            //if ($price > ($bidValue2[0] + $limitValue2[0]))
            if ($price > $bidValue2[0])
            {
                echo $price . " > " . $bidValue2[0] . " + " . $limitValue2[0]. '<br>';

                echo "user felhasználó id teszt: " . $userID2 . '<br>';
                    $this->sendMail($userID2, $productID, $price);
            }

        }
    }

    public function sendMail($userID, $productID, $price)
    {
        //echo "Felhasználó ID: ". $userID . '<br>';


        //email lekerdezese
        $conn = SqlConfig::connectToDatabase();
        $getMail = $conn->query("SELECT email FROM registeredusers WHERE userID='$userID'");
        $to = mysqli_fetch_row($getMail);
        //echo "The mail address " . $to[0] . '<br>';

        //termek info lekerdezese
        $getProduct = $conn->query("SELECT productName FROM products WHERE productID='$productID'");
        $productDetails = mysqli_fetch_row($getProduct);
        //echo "Termék neve: " . $productDetails[0] . '<br>';
        //echo "új ára: " . $price . '<br>';

        //email kuldese
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->SMTPAuth = true; //gmail authenticate
        $mail->SMTPSecure = 'ssl'; //connect with ssl

        $mail->Host = 'smtp.gmail.com'; //kiszolgáló
        $mail->Port = '465';
        $mail->isHTML();

        $mail->Username = 'WateraRendszerfejl@gmail.com';
        $mail->Password = 'watera2020rendszerfejl';

        $mail->setFrom('WateraRendszerfejl@gmail.com'); //From
        $mail->Subject  = 'Watera';
        $mail->Body = 'Határérték értesítés! Termék neve:   ' . $productDetails[0] . '   ára: ' . $price . 'Ft'; // Message

        $mail->AddAddress($to[0]); // to who

        if ( $mail->Send() )
        {
            echo "Email sent...";
        }
        else
        {
            echo "Error";
        }

        $mail->smtpClose();
    }

} // class END

?>