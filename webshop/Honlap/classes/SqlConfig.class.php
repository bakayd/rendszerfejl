<?php

class SqlConfig {

    private static $db_host = '127.0.0.1';
    private static $db_user = 'root';
    private static $db_password = '';
    private static $db_name = 'rendszerfejl';

    //Static methods can be called directly - without creating an instance of a class.
    //$conn = SqlConfig::connectToDatabase();
    public static function connectToDatabase()
    {
        $conn = new mysqli(self::$db_host, self::$db_user, self::$db_password, self::$db_name);
        if ($conn->connect_error)
        {
            die("Connection failed: " . $conn->connect_error);
        }

        return $conn;
    }
}
?>



































