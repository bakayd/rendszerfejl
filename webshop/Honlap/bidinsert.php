<html>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</html>

<?php
session_start();
require_once 'functions/init.functions.php';

$conn = SqlConfig::connectToDatabase();
if(isset($_GET['id']) && $_GET['id'] !== ''){
  $product_id = $_GET['id'];
  
} else {
}
/*if(isset($_GET['id']) && $_GET['id'] !== ''){
  $product_id = $_GET['id'];
  
} else {
  
}
//update products set `bidID`=bidID+1 where productID=16
//INSERT into bid (`bidID`,`productID`,`userID`,`bid`) values (2,16,3,2200)
$conn = SqlConfig::connectToDatabase();
        $sqlupdate = $conn->query("update products set `bidID`=bidID+1 where productID='$product_id'");
		$sqlinsert = $conn->query("INSERT into bid (`bidID`,`productID`,`userID`,`bid`) 
								values (select bidID from products order by bidID desc limit 1),$product_id,$_SESSION['userID'],$licit)");

  */


    if ( isset($_POST['submit']) )
    {
        //$productID = $_POST['productID'];
        $productID = $product_id;
		$userID = $_SESSION['userID'];
        $price = $_POST['price'];
        $pricetype = $_POST['pricetype'];
        if($pricetype=="$"){
            $price=$price*320;
        }
        else if($pricetype=="€"){
            $price=$price*350;
        }
        //$description = $_POST['description'];
        
        //$date = $_POST['bidEnd'];

        //termék userID
        $resultUserID = $conn->query("SELECT * FROM products WHERE productID = '$productID'");
        $rowUserID = $resultUserID->fetch_assoc();
        $prodUserID = $rowUserID['userID'];

        //saját termék check
        if ($prodUserID == $userID)
        {
            echo "<div class='container'><h1 class='bg-danger' style='text-align: center'>Saját termékre nem lehet licitálni!</h1></div>";
            $referrer = $_SERVER['HTTP_REFERER'];
            header("Refresh: 2;URL='$referrer'");
            return;
        }

        //aktuális licit
        $resultCurrBid = $conn->query("SELECT bid FROM `bid` WHERE productID='$product_id' ORDER by bid DESC LIMIT 1");
        $rowCurrBid = $resultCurrBid->fetch_assoc();
        $currBid = isset($rowCurrBid['bid'])?$rowCurrBid['bid']:0;


        //licitnek nagyobbnak kell lennie az aktuális licitnél/termék alapáránál
        if (($price <= $currBid) || ($price <= $rowUserID['price']))
        {
            echo "<div class='container'><h1 class='bg-danger' style='text-align: center'>A licitnek nagyobbnak kell lennie a termék áránál!</h1></div>";
            $referrer = $_SERVER['HTTP_REFERER'];
            header("Refresh: 2;URL='$referrer'");
            return;
        }



        //echo "Valtozok ellenorzese: " .$productID. " , " .$userID. " , " .$price . '<br>';

        $updateIfExists = $conn->query("SELECT * FROM bid WHERE productID='$productID' AND userID='$userID'");
        
        if ($updateIfExists->num_rows > 0)
            $sqlResult = "UPDATE bid SET bid = $price WHERE userID='$userID' and productID='$productID'";
        else
            $sqlResult = "INSERT into bid (`productID`,`userID`,`bid`) values ($productID,$userID,$price)";

        if(mysqli_query($conn, $sqlResult)) {
            echo "<div class='container'><h1 class='bg-success' style='text-align: center'>Sikeres licitálás!</h1></div>";
		} else {
            //echo "Error: " . $sqlResult . " " . $conn->error. '<br>';
        }
        
        //echo "Eljut" . '<br>';
		//határérték hozzáadása
        $limitValue = $_POST['limit'];
        $limitCLass = Limit::create();
        $limitCLass->addLimit($limitValue, $userID, $productID);

        //határértékek ellenörzése + értesítés
        $limitCLass->limitReachedNotify($price, $userID,$productID);

		//átirányítás előtt vár pár másoodpercet
		$referrer = $_SERVER['HTTP_REFERER'];
        header("Refresh: 2;URL='$referrer'");
	}
