<?php
session_start();
include "css/navbar.css.php";
require_once 'functions/init.functions.php';
?>

<!DOCTYPE html>
<html lang="hu">
<head>
    <title>Fizetés</title>
</head>
<body>
<?php
    /* echo 'session bidid: ' .$_SESSION['bidID']. '<br>';
    echo 'session product: ' .$_SESSION['productID']. '<br>'; */
    if ( isset($_POST['submit']) )
    {
        $product = Product::create();
        $product->payForProduct($_SESSION['bidID'], $_SESSION['productID']);
    }
?>
<div class="container">
    <h1>Fizetés</h1>
</div>

<div class="container">
    <form action="paying.php" method="post">
        <div class="form-group row">
            <label for="cardType" class="col-sm-3 col-form-label">Bankkártya tipusa:</label>
            <div class="col-sm-2">
                <select class="form-control" name="cardType" required>
                    <option value ="Mastercard">Mastercard</option>
                    <option value ="Visa">Visa</option>
                    <option value ="Maestro">Maestro</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
                <label for="cardNumber" class="col-sm-3 col-form-label">Kártya szám:</label>
            <div class="col-sm-3">
                <input type="number" class="form-control" name="cardNumber" pattern="[0-9]*" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength); validity.valid||(value='')"
                       maxlength = "16" style="display:table-cell;" required>
            </div>
        </div>

        <div class="form-group row" id="expiration">
            <label for="expiration" class="col-sm-3 col-form-label">Lejárati dátum:</label>
            <div class="col-sm-2">
                <select class="form-control">
                    <option value="01">Január</option>
                    <option value="02">Február </option>
                    <option value="03">Március</option>
                    <option value="04">Április</option>
                    <option value="05">Május</option>
                    <option value="06">Június</option>
                    <option value="07">Július</option>
                    <option value="08">Augusztus</option>
                    <option value="09">Szeptember</option>
                    <option value="10">Október</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                </select>
            </div>
                <div class="col-sm-2">
                    <select class="form-control">
                        <option value="16"> 2020</option>
                        <option value="17"> 2021</option>
                        <option value="18"> 2022</option>
                        <option value="19"> 2023</option>
                        <option value="20"> 2024</option>
                        <option value="21"> 2025</option>
                    </select>
                </div>
        </div>
        <div class="form-group row">
            <label for="cvc" class="col-sm-3 col-form-label">CVC:</label>
            <div class="col-sm-1">
                <input type="number" class="form-control" name="cvv" pattern="[0-9]" min="0" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength); validity.valid||(value='')"
                       maxlength = "3" style="display:table-cell;" required >
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-2">
                <input type="submit" class="btn btn-primary" name="submit" value="Fizetés">
            </div>
        </div>
    </form>
</div>


<div class="container">
    <form action="showWonProduct.php" method="post">
        <input type="submit" class="btn btn-primary" name="submit2" value="Termék megtekintése">
    </form>

</body>
</html>
