<?php
session_start();
include "css/navbar.css.php";
require_once 'functions/init.functions.php';
?>

<!DOCTYPE html>
<html lang="hu">

<head>
    <title>Termék hozzáadása</title>
</head>


<?php


if (isset($_POST['submit'])) {
    $productName = $_POST['productName'];
    $price = $_POST['price'];
    $pricetype = $_POST['pricetype'];
    if($pricetype=="$"){
        $price=$price*320;
    }
    else if($pricetype=="€"){
        $price=$price*350;
    }
    $description = $_POST['description'];
    $category = $_POST['category'];
    $bidEnd = $_POST['bidEnd'];
    $image = basename($_FILES["image"]["name"]);
    $userID = $_SESSION['userID'];
    //echo "Valtozok ellenorzese: " .$productName. " , " .$price. " , " .$description. " ,  " .$category. " , " .$bidEnd. " , " .$image. " , " .$userID. '<br>';


    //képfeltöltés
    $target_dir = "images/";
    $target_file = $target_dir . basename($_FILES["image"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
    $check = getimagesize($_FILES["image"]["tmp_name"]);
    if ($check !== false) {
        $uploadOk = 1;
    } else {
        echo "A választott fájl nem kép. ";
        $uploadOk = 0;
    }

    //kiterjesztés ellenőrzés
    if (
        $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif"
    ) {
        echo "Érvényes formátumok: JPG,PNG,JPEG,GIF. ";
        $uploadOk = 0;
    } else {
        //fájl méret ellenőrzés
        if ($_FILES["image"]["size"] > 5000000) {
            echo "A fájl mérete túl nagy. Maximum méret 5MB. ";
            $uploadOk = 0;
        } else {
            if ($uploadOk == 0) {
                echo "A fájl feltöltése sikertelen";
            } else {
                if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                    $product = Product::create();
                    $product->addData($productName, $price, $description, $category, $bidEnd, $image, $userID);
                    $product->addProduct();
                } else {
                    echo "Hiba történt a fájl feltöltése során";
                }
            }
        }
    }
}

?>

<body>
    <div class="container" style="margin-bottom: 10px">
        <h1>Termék hozzáadása</h1>
    </div>
    <div class="container">
        <form action="addProduct.php" method="post" name="addProductForm" enctype="multipart/form-data">
            <div class="row">
                <div class="form-group col-md-7">
                    <label for="productName">Termék neve</label>
                    <input type="text" class="form-control" name="productName" maxlength="24" required placeholder="Termék neve">
                </div>

                <div class="form-group col-md-4">
                    <label for="price">Ár</label>
                    <input type="number" pattern="[0-9]" min="0" oninput="validity.valid||(value='')" class="form-control" name="price" maxlength="11" style="display:table-cell;" required placeholder="Ár">
                </div>
                <div class="form-group col-md-1" style="margin-top:25px;">
                    <input class="lead" id="pricetype" name="pricetype" style="display:table-cell;border: 0px; background-color:white;width: 35%" value="Ft" readonly tabindex="-1">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col">
                    <label for="description">Termék leírása</label>
                    <textarea name="description" class="form-control" rows="8" cols="20" maxlength="500" required placeholder="Pár szó a termékről.."></textarea>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="category">Válassz kategóriát</label>
                    <select name="category" class="form-control">
                        <option value="1">Otthon</option>
                        <option value="2">Jármű</option>
                        <option value="3">Szolgáltatás</option>
                        <option value="4">Egyéb</option>
                    </select>
                </div>

                <div class="form-group col-md-4">
                    <label for="bidEnd">Licit érvényessége</label> <br>
                    <input type="datetime-local" class="form-control" id="bidEnd" name="bidEnd" min="<?php echo date('Y-m-d\TH:i'); ?>" required>
                </div>

                <div class="form-group col-md-4">
                    <label>Kép hozzáadása</label><br />
                    <input type="file" class="form-control custom-file-input" name="image" id="image" required>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                    <input type="submit" class="btn btn-primary" name="submit" value="Termék hozzáadása">
                </div>
            </div>
        </form>
    </div>
</body>

</html>
<script>
    $(document).ready(function() {
        var currenttype = "HUF";

        $('#huf').click(function(event) {
            
            currenttype = "HUF";
            document.getElementById("pricetype").value = "Ft";
        });
        $('#usd').click(function(event) {
            
            currenttype = "USD";
            document.getElementById("pricetype").value = "$";
        });

        $('#eur').click(function(event) {
            
            currenttype = "EUR";
            document.getElementById("pricetype").value = "€";
        });

    });
</script>