<?php
session_start();

if (isset($_POST['logout']) )
{
$_SESSION['logged_in'] = false;
session_unset();
session_destroy();

header("Location: ../index.php");
}

?>