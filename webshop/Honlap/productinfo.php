<?php
session_start();
include "css/navbar.css.php";
include "css/mainlist.css.php";
include "css/limitPopup.css.php";
include "script/currency.js.php";
require_once 'functions/init.functions.php';
$conn = SqlConfig::connectToDatabase();
if (isset($_GET['id']) && $_GET['id'] !== '') {
    $product_id = $_GET['id'];
    //echo $product_id;
} else {
    echo "failed";
}

$sqlResult = $conn->query("SELECT * FROM products WHERE productID='$product_id'");

if ($sqlResult->num_rows == 0)
    echo "Nincs ilyen termék";
else {
    $productData = $sqlResult->fetch_assoc();

    $productID = $productData['productID'];
    $productName = $productData['productName'];
    $price = $productData['price'];
    $description = $productData['description'];
    $categoryID = $productData['categoryID'];
    $userID = $productData['userID'];
    $endDate = $productData['endDate'];
    $image = 'images/' . $productData['image'];
    $payed = $productData['payed'];
};
?>

<!DOCTYPE html>
<html>

<head>
    <title>Termékinfó</title>
    <style>
        hr {
            width: 100%;
            color: black;
            height: 1px;
            background-color:black;
        }
    </style>
</head>

<body>
<div class="container">
    <div class="row">

        <div class="col-md-5 well well-sm">
            <img class="img-responsive " src='<?php echo $image ?>' alt='kép a termékről'/>
        </div>

        <div class="col-md-7">
            <div class="col-md-10 well well-sm">

                    <div class="row">
                        <div class="col-md-12" style="margin-bottom: 10px">
                            <div class="col-md-12">
                                <h1><?php echo $productName ?></h1>
                            </div>
                            <hr>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-5">
                                <p class="lead">Kikiáltási ár:</p>
                            </div>
                            <div class="col-md-4">
                                <p class="lead price"><?php echo $price ?></p>
                            </div>
                            <div class="col-md-1">
                                <p class="lead pricetype">Ft</p>
                            </div>
                            <hr>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-5">
                                <p class="lead">Leírás:</p>
                            </div>
                            <div class="col-md-7" style="margin-bottom: 10px">
                                <?php echo $description ?>
                            </div>
                            <hr>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-5">
                                <p class="lead">Licit vége:</p>
                            </div>
                            <div class="col-md-7">
                                <p class="lead"><?php echo $endDate; ?></p>
                                <p class="small text-center" id="bidtimer"></p>
                            </div>
                            <hr>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-5">
                                <p class="lead">Aktuális licit:</p>
                            </div>
                            <div class="col-md-7">
                                <p class="lead" id="demo"></p>
                            </div>
                            <hr style="width: 100%; color: black; height: 1px; background-color:black;">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12" style="margin-left: 10px">
                            <form action="bidinsert.php?id=<?php echo $product_id; ?>" method="post" name="licit">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="popup" onclick="popup()"><a>Mi ez?</a>
                                            <span class="popuptext" id="myPopup">Ha szeretne értesítést kapni amikor rálicitálnak az ön által megadott összegre,
                                    akkor a licit megtételekor adjon meg egy az ön által beállított licitösszegnél nagyobb határértéket.
                                    Ha a licit túllépi a megadott határértéket email-ben értesítjük.</span>
                                        </div>
                                        <p class="lead">Határérték:</p>
                                    </div>

                                    <div class="col-md-8" style="margin-top: 10px">
                                        <div class="col-md-9">
                                            <input type="number" class="form-control" name="limit" maxlength="11"
                                                   style="display:table-cell;" placeholder="Nem kötelező">
                                        </div>
                                        <div class="col-md-3">
                                            <input class="lead pricetype" name="pricetype"
                                                   style="display:table-cell;border: 0px; background-color:transparent;width: 45%;"
                                                   value="Ft" readonly tabindex="-1">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="lead" for="price">Ár:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="col-md-9">
                                            <input type="number" class="form-control" name="price" style="display:table-cell;"
                                                   required>
                                        </div>
                                        <div class="col-md-3">
                                            <input class="lead pricetype" name="pricetype"
                                                   style="display:table-cell;border: 0px; background-color:transparent;width: 45%;"
                                                   value="Ft" readonly tabindex="-1">
                                        </div>
                                    </div>
                                </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12" style="margin-left: 10px">
                            <input type="submit" class="btn btn-primary" id="submit" name="submit" value="Licit">
                            <!-- <input type="submit" name="submit" value="Licit"> -->
                        </div>
                    </div>

                    </form>
                </div>


                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
                <script type="text/javascript">
                    var time = 0;
                    var endDate = 0;
                    var autoload = setInterval(
                        function () {
                            $('#demo').load('bidinfo.php?id=<?php echo $product_id; ?>').fadeIn("Slow");
                            $('#bidtimer').load('bidtimer.php?id=<?php echo $product_id; ?>').fadeIn("Slow");
                            //document.getElementById("load").innerHTML = licitlimit;
                        }, 1000);


                    function licitoff() {
                        document.getElementById("submit").disabled = true;
                    };
                </script>
                <div id="load">
                </div>
            </div>
        </div>
    </div>
</div>
</body>

</html>

<!--limit popup-->
<script>
    // When the user clicks on div, open the popup
    function popup() {
        var popup = document.getElementById("myPopup");
        popup.classList.toggle("show");
    }
</script>
