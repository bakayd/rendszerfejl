<!DOCTYPE html>
<html lang="hu">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <meta charset="UTF-8">
    <style>
        .navbar-brand{
            font-size: 30px;
        }
        .navbar-nav {
            position: absolute;
            left: 50%;
            transform: translatex(-50%);
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.php">Watera</a>
            </div>
            <ul class="nav navbar-nav">
                <li><a href="index.php">Kezdőlap</a></li>
                <li><a href="loginPage.php">Bejelentkezés</a></li>
                <li>
                    <form class="navbar-form navbar-left" action="/action_page.php"> <!--TODO PHP-->
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Keresés..." name="search">
                        </div>
                        <button type="submit" class="btn btn-default">Keresés</button>
                    </form>
                </li>
            </ul>
        </div>
    </nav>
</body>
</html>

<?php

?>
