<?php
session_start();
include "navbar.php";
require('sqlConfig.php');
?>

<!DOCTYPE html>
<html lang="hu">
<head>
    <title>Login</title>
</head>

<body>
<div class="container" style="margin-bottom: 10px">
    <h2>Bejelentkezés</h2>
<?php
if ( isset($_POST['submit']) )
{
    $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $email = $_POST['email'];
    $sqlResult = $conn->query("SELECT * FROM registeredusers WHERE email='$email'");

    if ($sqlResult->num_rows == 0)
    {
        echo "Nincs ilyen felhasználó";
        //$_SESSION['message'] = "Nincs ilyen felhasználó regisztrálva a rendszerben";
        //header("Location: index.php");
    }
    else
    {
        $userData = $sqlResult->fetch_assoc();

        if (md5($_POST['password']) == $userData['password'])
        {
            $_SESSION['email'] = $userData['email'];
            $_SESSION['name'] = $userData['name'];
            $_SESSION['logged_in'] = true;

            header("Location: loggedInUserPage.php");
        }
        else
            //print_r($_SESSION);
            echo "Rossz jelszót adott meg";
    }
}
?>
</div>
<div class="container">
    <form action="loginPage.php" method="post" name="form" >
        <div class="registration">
            <label for="email">Email:</label>
            <input type="email" name="email" required> <br> <br>

            <label for="password">Jelszó:</label>
            <input type="password" name="password" required> <br> <br>

            <input type="submit" name="submit" value="Bejelentkezés"> <br><br>

            <label for="ujFiok">Nincs fiókod? Hozz létre egyet:</label>
            <a href="registrationForm.php">Fiók létrehozása</a>
        </div>
    </form>
</div>

</body>
</html>