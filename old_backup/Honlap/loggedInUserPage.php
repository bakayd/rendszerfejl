<?php
    include "navbar.php";
	session_start();
?>

<!DOCTYPE html>
<html lang="hu">
<head>
	<title>Login</title>
</head>

<body>
<div class="container">
<?php
	if ( isset($_SESSION['logged_in']) )
	{
		echo "Üdvözöljük " .$_SESSION['name']. "";
	}

	if (isset($_POST['logout']) )
	{
		$_SESSION['logged_in'] = false;
		session_unset();
		//session_destroy();

		header("Location: index.php");
	}
?>

<form action="loggedInUserPage.php" method="post" name="form">
	<br><br> <input type="submit" name="logout" value="Kijelentkezés">
</form>
</div>

</body>
</html>