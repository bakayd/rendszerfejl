<?php
    include "navbar.php";
    require_once('sqlConfig.php');
?>

<!DOCTYPE html>
<html lang="hu">
<head>
    <title>Regisztráció</title>
</head>
<body>
        <?php
            if ( isset($_POST['submit']) )
            {
                if ($_POST['password'] == $_POST['password2'])
                {
                    $nev = $_POST['name'];
                    $email = $_POST['email'];
                    $password =  md5($_POST['password']); // md5 password encryption
                    $mobileNumber = ($_POST['mobileNumber'] ? $_POST['mobileNumber'] : '');
                    $lakcim = ($_POST['address'] ? $_POST['address'] : '');
                    $birthday = $_POST['birthday'];

                    //echo "Valtozok ellenorzese: " .$nev. " , " .$email. " , " .$password. " ,  " .$mobileNumber. " , " .$lakcim.  "" .$birthdayYear. "" .$birthdayMonth. "" .$birthdayDay. "";

                    $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
                    if ($conn->connect_error) {
                        die("Connection failed: " . $conn->connect_error);
                    }

                    $matchMail = $conn->query("SELECT * FROM registeredusers WHERE email='$email'");

                    if ( $matchMail->num_rows > 0 )
                        echo "Már van regisztrála felhasználó az alábbi email címmel: " .$email. "";
                    else
                        {
                            $sql = "INSERT INTO registeredusers (name, email, password, mobileNumber, address, birthday) 
                            VALUES ('$nev', '$email', '$password', '$mobileNumber', '$lakcim', '$birthday')";

                            if(mysqli_query($conn, $sql)){
                                echo "Records added successfully.";
                                header("Location: loginPage.php");
                            } else
                                echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
                        }
                    $conn->close();
                }
                else
                    echo "A ket jelszo nem egyezik meg";
            }
        ?>

    <div>
        <form action="registrationForm.php" method="post">
            <div class="container">
                <h1>Regisztráció</h1>

                <label for="name" >Név: *</label><br>
                <input type="text" name="name" required> <br><br>

                <label for="email" >Email cím: *</label><br>
                <input type="email" name="email" required> <br><br>

                <label for="password">Jelszó: *</label> <br>
                <input type="password" name="password" required> <br><br>

                <label for="password2">Jelszó még egyszer: *</label> <br>
                <input type="password" name="password2" required> <br><br>

                <label for="mobileNumber">Mobiltelefonszám:</label> <br>
                <input type="tel" name="mobileNumber">  <br><br>

                <label for="address" >Lakcím:</label><br>
                <input type="text" name="address"> <br><br>

                <label for="birthday">Születési dátum:</label> <br>
                <input type="date" name="birthday" min="1900-01-01" max="2020-04-07"> <br><br>

                <input type="checkbox" name="userAgreement" required>
                <label for="userAgreement">Felhasználói feltételek elfogadása</label><br><br>

                <input type="submit" name="submit" value="Submit">
                
            </div>
          </form> 
        
    </div>

</body>
</html>
